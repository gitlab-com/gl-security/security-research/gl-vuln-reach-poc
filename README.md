> Marks vulnerabilities in dependencies that are not reachable from the calling application as false positives

To see this PoC in action, watch the [demo](https://youtu.be/ko4P4r7VN60) and see the example [pipeline](https://gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/extracteur/-/pipelines/1226585773/security?reportType=DEPENDENCY_SCANNING).

## Usage

As part of a CI pipeline:

```yaml
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

gemnasium-dependency_scanning:
  after_script:
    - go install golang.org/x/vuln/cmd/govulncheck@latest
    - find / -name govulncheck
    - /tmp/go/bin/govulncheck --json ./... > govulncheck-report.json
    - npm config set @gitlab-com:registry=https://gitlab.com/api/v4/groups/6543/-/packages/npm/
    - npx @gitlab-com/gl-vuln-reach-poc gl-dependency-scanning-report.json govulncheck-report.json
    - mv gl-dependency-scanning-reachable-report.json gl-dependency-scanning-report.json
```

Locally:

```sh
node index.js path/to/gl-dependency-scanning-report.json path/to/govulncheck-output.json
```