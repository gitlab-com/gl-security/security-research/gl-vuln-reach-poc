#!/usr/bin/env node
'use strict'

const Ajv = require('ajv')
const ajv = new Ajv()
const fs = require('fs')
const path = require('path')

const { chain } = require('stream-chain')
const { parser } = require('stream-json')
const { streamValues } = require('stream-json/streamers/StreamValues')

// const util = require('node:util')
// const exec = util.promisify(require('node:child_process').exec)

;(async function () {
  const reportFile = process.argv[2]
  const vulncheckFile = process.argv[3]
  console.log(`checking ${reportFile} for reachable vulnerabilities`)
  const report = JSON.parse(fs.readFileSync(reportFile))

  validateReport(report)

  const cves = getIdentifiers(report)

  const vcReport = await parseVulncheckReport(vulncheckFile)
  const reachableFindings = findReachableFindings(vcReport)

  for (const cve of cves) {
    const identifiers = cve.map(x => x.value)

    const isReachable = identifiers.some(x => reachableFindings.has(x))
    if (isReachable) {
      console.log(`${cve[0].value} is reachable`)
    } else {
      console.log(`${cve[0].value} is not reachable`)
    }
    addReachabilityInfo(report, cve[0].value, isReachable)
  }

  validateReport(report)
  fs.writeFileSync(
    'gl-dependency-scanning-reachable-report.json',
    JSON.stringify(report, null, 2)
  )
  console.log('done')
})()

function validateReport (report) {
  const schema = JSON.parse(
    fs.readFileSync(
      path.join(__dirname, 'dependency-scanning-report-format.json'),
      'utf-8'
    )
  )
  const validate = ajv.compile(schema)
  if (validate(report) === false) {
    console.error(`report does not match schema definition`)
    console.error(validate.errors)
    process.exit(1)
  }
}

function getIdentifiers (report) {
  return report.vulnerabilities.map(x =>
    x.identifiers.filter(i => i.type === 'cve' || i.type === 'ghsa')
  )
}

async function parseVulncheckReport (filepath) {
  const pipeline = chain([
    fs.createReadStream(filepath),
    parser({ jsonStreaming: true }),
    streamValues()
  ])

  const report = []
  for await (const { value } of pipeline) {
    const keys = Object.keys(value)
    if (
      keys.length !== 1 ||
      ['finding', 'osv', 'progress', 'config'].includes(keys[0]) === false
    ) {
      throw new Error(`Unknown key ${keys}`)
    }

    const k = keys.pop()
    if (report[k] === undefined) report[k] = []
    report[k].push(value[k])
  }
  return report
}

function findReachableFindings (vulncheckReport) {
  // Get ids of all reachable findings
  const ids = vulncheckReport.finding
    .filter(x => x.trace?.some(t => t.position))
    .map(x => x.osv)
  const uniqueIds = [...new Set(ids)]

  // Get aliases of all reachable findings
  const aliases = new Set()
  uniqueIds.forEach(id => {
    const osv = vulncheckReport.osv.find(x => x.id === id)
    aliases.add(osv.aliases)
  })

  // return id and aliases of reachable findings
  return new Set(uniqueIds.concat(...aliases))
}

function addReachabilityInfo (report, id, isReachable) {
  if (isReachable === true) return

  const v = report.vulnerabilities.find(x =>
    x.identifiers.some(i => i.value === id)
  )
  v.flags = [
    {
      type: 'flagged-as-likely-false-positive',
      origin: 'govulncheck',
      description:
        'The vulnerable dependency code is not reachable from the calling application.'
    }
  ]
}
